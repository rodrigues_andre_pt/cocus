package pt.cocus.exchange.rate.bridge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import pt.cocus.common.exception.CocusException;
import pt.cocus.common.util.DateUtil.DatePattern;
import pt.cocus.exchange.cache.ExchangeRateCacheManager;
import pt.cocus.exchange.rate.api.ExchangeRateApi;

/**
 * Test class for {@link ExchangeRateBridge}
 * 
 * @author andrerodrigues
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateBridgeTest {
	@Mock
	private ExchangeRateBridge bridgeMock;
	private String EURO = "EUR";

	@Test
	public void TestRetrieveFromCache() {
		ExchangeRateBridge bridge = ExchangeRateBridge.getInstance();
		assertNull(bridge.retrieveFromCache(null));
		ExchangeRateCacheManager cacheManager = ExchangeRateCacheManager.getInstance();
		assertNotNull(cacheManager);
		cacheManager.clear();
		assertNull(bridge.retrieveFromCache(EURO));

		// Test add an older exchange rate, it has to be
		// deleted due to be out date.
		ExchangeRateApi api = new ExchangeRateApi();
		api.setDate("2010-01-01");
		cacheManager.put(EURO, api);
		assertNotNull(cacheManager.get(EURO));
		assertNull(bridge.retrieveFromCache(EURO));
		assertNull(cacheManager.get(EURO));

		// Test add an updated exchange rate, and ensure
		// that stays in cache
		api.setDate(new SimpleDateFormat(DatePattern.YYYY_MONTH_DAY_HYPHEN.getPattern()).format(new Date()));
		cacheManager.put(EURO, api);
		assertNotNull(cacheManager.get(EURO));
		assertEquals(api, bridge.retrieveFromCache(EURO));
		assertNotNull(cacheManager.get(EURO));
	}

	@Test
	public void TestRetrieveExchangeRate() throws IOException, CocusException {
		ExchangeRateCacheManager cacheManager = ExchangeRateCacheManager.getInstance();
		cacheManager.clear();
		assertNull(cacheManager.get(EURO));

		StringBuilder httpResult = new StringBuilder("{\"rates\":{\"USD\":1.1169},\"base\":\"EUR\",\"date\":\"2020-01-16\"}");
		Mockito.when(bridgeMock.httpCall(Mockito.anyString())).thenReturn(httpResult);

		Mockito.when(bridgeMock.retrieveExchangeRate(Mockito.anyString())).thenCallRealMethod();
		ExchangeRateApi api = bridgeMock.retrieveExchangeRate(EURO);
		assertNotNull(api);
		assertEquals("EUR", api.getBase());
		assertEquals("2020-01-16", api.getDate());
		assertNotNull(api.getRates());
		assertEquals(1, api.getRates().size());
		assertNotNull(api.getRates().get("USD"));
		assertEquals(new Float(1.1169), api.getRates().get("USD"));
	}
}
