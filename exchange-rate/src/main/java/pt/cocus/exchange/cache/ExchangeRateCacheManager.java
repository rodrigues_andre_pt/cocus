package pt.cocus.exchange.cache;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import pt.cocus.exchange.rate.api.ExchangeRateApi;

/**
 * Singleton class that implements cache on exchange rate project.
 * The exchange rate is cached in a map with currency as key.
 * 
 * @author andrerodrigues
 *
 */
public class ExchangeRateCacheManager {
	private static ExchangeRateCacheManager instance;
	private Map<String, ExchangeRateApi> cache = Collections.synchronizedMap(new HashMap<String, ExchangeRateApi>());

	private ExchangeRateCacheManager() {
		// Hide constructor
	}

	/**
	 * Method that instantiate a Singleton instance of the class
	 * 
	 * @return
	 */
	public static ExchangeRateCacheManager getInstance() {
		if (instance == null) {
			synchronized (ExchangeRateCacheManager.class) {
				if (instance == null) {
					instance = new ExchangeRateCacheManager();
				}
			}
		}
		return instance;
	}

	public void put(String key, ExchangeRateApi value) {
		cache.put(key, value);
	}

	public ExchangeRateApi get(String key) {
		return cache.get(key);
	}

	public void remove(String key) {
		cache.remove(key);
	}

	public void clear() {
		cache.clear();
	}
}
