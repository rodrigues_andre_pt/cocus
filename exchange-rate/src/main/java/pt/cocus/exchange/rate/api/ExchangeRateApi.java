package pt.cocus.exchange.rate.api;

import java.util.Map;

/**
 * Class that represents the response of exchange rate api
 * 
 * @author andrerodrigues
 *
 */
public class ExchangeRateApi {
	private Map<String, Float> rates;
	private String base;
	private String date;

	/**
	 * @return the rates
	 */
	public Map<String, Float> getRates() {
		return rates;
	}

	/**
	 * @param rates the rates to set
	 */
	public void setRates(Map<String, Float> rates) {
		this.rates = rates;
	}

	/**
	 * @return the base
	 */
	public String getBase() {
		return base;
	}

	/**
	 * @param base the base to set
	 */
	public void setBase(String base) {
		this.base = base;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
}
