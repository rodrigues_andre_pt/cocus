package pt.cocus.exchange.rate.message;

import pt.cocus.logs.message.Message;

/**
 * Enumeration of error messages
 * 
 * @author andrerodrigues
 *
 */
public enum ExchangeRateMessage implements Message {
	ERROR_CALLING(3001, "Error calling exchange rate api at {}", Severity.ERROR);

	private final int code;
	private final String detail;
	private Severity severity;

	private ExchangeRateMessage(int code, String detail, Severity severity) {
		this.code = code;
		this.detail = detail;
		this.severity = severity;
	}

	@Override
	public String getDetail() {
		return detail;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public Severity getSeverity() {
		return severity;
	}
}
