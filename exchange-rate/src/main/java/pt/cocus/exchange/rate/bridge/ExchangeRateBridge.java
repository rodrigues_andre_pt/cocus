package pt.cocus.exchange.rate.bridge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.google.gson.Gson;

import pt.cocus.common.exception.CocusException;
import pt.cocus.common.util.DateUtil.DatePattern;
import pt.cocus.exchange.cache.ExchangeRateCacheManager;
import pt.cocus.exchange.rate.api.ExchangeRateApi;
import pt.cocus.exchange.rate.message.ExchangeRateMessage;
import pt.cocus.logs.util.CocusLogging;

/**
 * Singleton class that implements the bridge to communicate with exchange rate api.
 * 
 * @author andrerodrigues
 *
 */
public class ExchangeRateBridge {
	private static final String URL = "https://api.exchangeratesapi.io/latest?symbols=";
	private static final int HTTP_CONNECT_TIMEOUT = 5000;
	private static final int HTTP_READ_TIMEOUT = 5000;
	private static ExchangeRateBridge instance = null;
	private Gson gson;
	
	private ExchangeRateBridge() {
		// Avoid initialization, force use getInstance
	}
	
	/**
	 * Creates a instance of the class
	 * 
	 * @return
	 */
	public static ExchangeRateBridge getInstance() {
		if (instance == null) {
			synchronized (ExchangeRateBridge.class) {
				if (instance == null) {
					instance = new ExchangeRateBridge();
				}
			}
		}
		return instance;
	}
	
	/**
	 * Method that performs a HTTP call to external api and return
	 * the api of the result
	 * 
	 * @param code currency code to get the exchange rate
	 * @return
	 * @throws CocusException
	 */
	public ExchangeRateApi retrieveExchangeRate(String code) throws CocusException {
		if (code == null) {
			throw new CocusException("Missing currency code to retrieve exchange rate");
		}
		ExchangeRateApi rate = retrieveFromCache(code);
		if (rate != null) {
			return rate;
		}
		StringBuilder targetUrl = new StringBuilder(URL).append(code);
		try {
			StringBuilder content = httpCall(targetUrl.toString());
			if (content == null) {
				return null;
			}
			ExchangeRateApi rateApi = getGson().fromJson(content.toString(), ExchangeRateApi.class);
			ExchangeRateCacheManager.getInstance().put(code, rateApi);
			return rateApi;
		} catch (Exception e) {
			throw new CocusException(new StringBuilder("Error calling: ").append(targetUrl).toString(), e);
		}
	}

	/**
	 * Method to perform HTTP call
	 * 
	 * @param targetUrl url to call
	 * @return
	 * @throws IOException
	 */
	protected StringBuilder httpCall(String targetUrl) throws IOException {
		URL url = new URL(targetUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(HttpMethod.GET.name());
		con.setRequestProperty(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.getType());
		con.setConnectTimeout(HTTP_CONNECT_TIMEOUT);
		con.setReadTimeout(HTTP_READ_TIMEOUT);
		con.setInstanceFollowRedirects(false);
		int status = con.getResponseCode();
		if (HttpStatus.OK.value() != status) {
			CocusLogging.writeLog(getClass(), ExchangeRateMessage.ERROR_CALLING, url);
			return null;
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuilder content = new StringBuilder();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();
		con.disconnect();
		return content;
	}

	/**
	 * Method that retrieves the exchange rate from cache
	 * if exists and if is valid
	 * 
	 * @param code currency code to get exchange rate
	 * @return
	 */
	protected ExchangeRateApi retrieveFromCache(String code) {
		if (code == null) {
			return null;
		}
		ExchangeRateApi value = ExchangeRateCacheManager.getInstance().get(code);
		if (value != null) {
			// Check if cache is valid
			String currentDate = new SimpleDateFormat(DatePattern.YYYY_MONTH_DAY_HYPHEN.getPattern()).format(new Date());
			if (!currentDate.equals(value.getDate())) {
				// Invalid cache
				ExchangeRateCacheManager.getInstance().remove(code);
			} else {
				return value;
			}
		}
		return null;
	}
	
	/**
	 * Lazy load of Gson
	 * 
	 * @return
	 */
	private Gson getGson() {
		if (gson == null) {
			gson = new Gson();
		}
		return gson;
	}
}
