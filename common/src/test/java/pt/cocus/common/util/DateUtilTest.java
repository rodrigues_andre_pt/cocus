package pt.cocus.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import java.text.ParseException;

import org.junit.Test;

import pt.cocus.common.util.DateUtil.DatePattern;

/**
 * Test class for {@link DateUtil}
 * 
 * @author andrerodrigues
 *
 */
public class DateUtilTest {
	
	@Test
	public void TestConvertFromToFormat() throws ParseException {
		assertNull(DateUtil.convertFromToFormat(null, null, null));
		assertNull(DateUtil.convertFromToFormat("", null, null));
		assertNull(DateUtil.convertFromToFormat("a", null, null));
		assertNull(DateUtil.convertFromToFormat("a", DatePattern.DAY_MONTH_YEAR, null));
		assertNull(DateUtil.convertFromToFormat("a", null, DatePattern.DAY_MONTH_YEAR));
		
		assertEquals("2010/01/02", DateUtil.convertFromToFormat("02/01/2010", DatePattern.DAY_MONTH_YEAR, DatePattern.YEAR_MONTH_DAY));
		assertEquals("02/01/2010", DateUtil.convertFromToFormat("2010/01/02", DatePattern.YEAR_MONTH_DAY, DatePattern.DAY_MONTH_YEAR));
	}
	
	@Test
	public void TestDateBeforeOf() {
		assertFalse(DateUtil.dateBeforeOf(null, null, null));
		assertFalse(DateUtil.dateBeforeOf("", null, null));
		assertFalse(DateUtil.dateBeforeOf(null, "", null));
		assertFalse(DateUtil.dateBeforeOf("a", null, null));
		assertFalse(DateUtil.dateBeforeOf(null, "a", null));
		assertFalse(DateUtil.dateBeforeOf("a", "", null));
		assertFalse(DateUtil.dateBeforeOf("", "a", null));
		assertFalse(DateUtil.dateBeforeOf("a", "a", null));

		assertTrue(DateUtil.dateBeforeOf("2010/01/02", "2010/01/03", DatePattern.YEAR_MONTH_DAY));
		assertFalse(DateUtil.dateBeforeOf("2010/01/02", "2010/01/01", DatePattern.YEAR_MONTH_DAY));
	}
	
	@Test
	public void TestValidDate() {
		assertFalse(DateUtil.validDate(null, null));
		assertFalse(DateUtil.validDate("", null));
		assertFalse(DateUtil.validDate("a", null));
		
		assertTrue(DateUtil.validDate("2010/10/01", DatePattern.YEAR_MONTH_DAY));
		assertFalse(DateUtil.validDate("invalid_date", DatePattern.DAY_MONTH_YEAR));
	}
}
