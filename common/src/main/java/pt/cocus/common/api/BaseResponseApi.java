package pt.cocus.common.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Class that represents the base of all WS responses.
 * All responses should extend this class.
 * 
 * @author andrerodrigues
 * 
 */
@JsonInclude(Include.NON_NULL)
public class BaseResponseApi {
	private boolean success;
	private ResponseError error;

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the error
	 */
	public ResponseError getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(ResponseError error) {
		this.error = error;
	}
}
