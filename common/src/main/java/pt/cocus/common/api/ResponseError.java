package pt.cocus.common.api;

/**
 * Class that represents the error object in WS responses.
 * 
 * @author andrerodrigues
 *
 */
public class ResponseError {
	private String code;
	private String detail;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}
}
