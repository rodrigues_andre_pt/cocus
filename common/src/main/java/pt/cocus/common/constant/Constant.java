package pt.cocus.common.constant;

/**
 * Class that represents a String pool. The objective is avoid
 * multiple declarations of the same string in JVM.
 * 
 * @author andrerodrigues
 * 
 */
public class Constant {
	
	private Constant() {
		// Avoid initialization
	}
	
	public static final String DEST = "dest";
	public static final String ORIG = "orig";
	public static final String DATE_FROM = "dateFrom";
	public static final String DATE_TO = "dateTo";
	public static final String CURR = "curr";
	public static final String ID = "id";
	public static final String EQUALS = "=";
	public static final String AND_CODE = "&";
	public static final String AIRPORT = "airport";
	public static final String COMMA = ",";
	public static final String EMPTY_STRING = "";
	public static final String LOG_PLACEHOLDER = "\\{\\}";
	public static final String ERROR_PLACEHOLDER = "\\{0\\}";
	public static final String FLY_FROM = "flyFrom";
	public static final String FLY_TO = "to";
	public static final String QUERY_PARAM_SYMBOL = "?";
}
