package pt.cocus.common.workflow;

import pt.cocus.common.validator.BaseValidator;
import pt.cocus.common.filter.BaseFilter;

/**
 * Interface that all workflows must implement to ensure coherence
 * in the architecture.
 * 
 * @author andrerodrigues
 *
 * @param <T>
 */
public interface BaseWorkflow<T> extends BaseValidator<BaseFilter> {
	public T getValidator();
}
