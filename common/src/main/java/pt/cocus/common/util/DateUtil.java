package pt.cocus.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * Class that contains utils methods to manipulate dates
 * 
 * @author andrerodrigues
 *
 */
public class DateUtil {
	/**
	 * Enumeration of date formats
	 */
	public enum DatePattern {
		YEAR_MONTH_DAY("yyyy/MM/dd"),
		DAY_MONTH_YEAR("dd/MM/yyyy"),
		YYYY_MONTH_DAY_HYPHEN("yyyy-MM-dd");
		
        private String pattern;

        DatePattern(String pattern) {
            this.pattern = pattern;
        }

        public String getPattern() {
            return pattern;
        }
	}
	
	/**
	 * Return the date received from a date pattern to another date pattern.
	 * 
	 * @param date date to convert
	 * @param from current pattern
	 * @param to pattern to return
	 * @return String with date in new pattern
	 * @throws ParseException exception thrown if date is in wrong format
	 */
	public static String convertFromToFormat(String date, DatePattern from, DatePattern to) throws ParseException {
		if (StringUtils.isEmpty(date) || from == null || to == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(from.getPattern());
		Date d = sdf.parse(date);
		sdf.applyPattern(to.getPattern());
		return sdf.format(d);
	}
	
	/**
	 * Checks if dates are valid and if dateFrom is before dateTo
	 * 
	 * @param dateFrom start date
	 * @param dateTo end date
	 * @param pattern  
	 * @return
	 */
	public static boolean dateBeforeOf(String dateFrom, String dateTo, DatePattern pattern) {
		if (StringUtils.isEmpty(dateFrom) || StringUtils.isEmpty(dateTo) || pattern == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern.getPattern());
		try {
			Date firstDate = sdf.parse(dateFrom);
			Date secondDate = sdf.parse(dateTo);
			if (firstDate.after(secondDate)) {
				return false;
			}
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if date is in request format
	 * 
	 * @param date date
	 * @param pattern format to check
	 * @return
	 */
	public static boolean validDate(String date, DatePattern pattern) {
		if (StringUtils.isEmpty(date) || pattern == null) {
			return false;
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern.getPattern());
			sdf.parse(date);
		} catch (ParseException e) {
			// Invalid date
			return false;
		}
		return true;
	}
}
