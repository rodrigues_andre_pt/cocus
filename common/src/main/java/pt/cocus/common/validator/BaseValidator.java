package pt.cocus.common.validator;

import pt.cocus.common.api.ResponseError;
import pt.cocus.common.filter.BaseFilter;

/**
 * Interface extended by BaseWorkflow that is implemented by
 * all workflows.
 * For now it contains just a "validate" method to validate
 * the input.
 * 
 * @author andrerodrigues
 *
 * @param <T>
 */
public interface BaseValidator<T extends BaseFilter>  {
	
	public abstract boolean validate(T filter, ResponseError error);
}
