package pt.cocus.logs.message;

/**
 * Generic message structure that is implemented from specific messages. Used
 * for Exceptions and logging
 * 
 * @author andrerodrigues
 * 
 */
public interface Message {

	public enum Severity {
		ERROR, WARNING, INFO, DEBUG, TRACE
	}

	public int getCode();

	public String getDetail();

	public Severity getSeverity();
}
