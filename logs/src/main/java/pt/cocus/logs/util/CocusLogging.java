package pt.cocus.logs.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pt.cocus.logs.message.Message;

/**
 * Logging class.
 * 
 * It saves in memory the log instance of all classes.
 * 
 * @author andrerodrigues
 *
 */
public class CocusLogging {
	private static Map<Class<?>, Logger> logInstances = new HashMap<>();

	private CocusLogging() {
		// Avoid initialization
	}

	/**
	 * Method to write a log message
	 * 
	 * @param classInstance class that is logging the message
	 * @param messageToLog message to log
	 * @param args message's parameters
	 */
	public static void writeLog(Class<?> classInstance, Message messageToLog, Object... args) {
		// Get the correct logger for this class
		Logger logger = getLogger(classInstance);
		
		// String formatting is only applied if needed (if log level is enabled)
		switch (messageToLog.getSeverity()) {
		case ERROR:
			logger.error(messageToLog.getDetail(), args);
			break;
		case DEBUG:
			logger.debug(messageToLog.getDetail(), args);
			break;
		case INFO:
			logger.info(messageToLog.getDetail(), args);
			break;
		case TRACE:
			logger.trace(messageToLog.getDetail(), args);
			break;
		case WARNING:
			logger.warn(messageToLog.getDetail(), args);
			break;
		default:
			logger.error(messageToLog.getDetail(), args);
			break;
		}
	}

	/**
	 * Method to write a exception
	 * 
	 * @param classInstance class that is logging the message
	 * @param e exception thrown
	 * @param args additional info
	 */
	public static void writeException(Class<?> classInstance, Throwable e, Object... args) {
		// Get the correct logger for this class
		Logger logger = getLogger(classInstance);

		// If there's some additional info like input params for the method that raised
		// the exception, log it
		if (logger.isErrorEnabled() && args != null && args.length > 0) {
			logger.error(String.format("%1$s - Detail: %2$s", e.getLocalizedMessage(), Arrays.toString(args)), e);
		} else {
			// Log exception
			logger.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Method that gets the log instance from memory (if already initialized).
	 * Otherwise the log is initialized and saved in memory to be reused.
	 * 
	 * @param classInstance class that is logging the message
	 * @return logger instance
	 */
	private static Logger getLogger(Class<?> classInstance) {
		Logger logger = logInstances.get(classInstance);

		if (logger == null) {
			synchronized (logInstances) { // mutex
				if (logger == null) {
					// First time logging for this class, instantiates new logger and add to map
					logger = LoggerFactory.getLogger(classInstance);
					logInstances.put(classInstance, logger);
				}
			}
		}
		return logger;
	}
}
