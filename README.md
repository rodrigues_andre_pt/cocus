## Code challenge - logic implemented

# Architecture
It were the following eight projects:

1. Modules
2. Parent
3. Common
4. Logs
5. Exchange Rate
6. Flights Client
7. Flights DB
8. Flights

# Explanation
1. The project modules (pom) is to compile all the application projects.

2. The parent project (pom) is where is defined all the maven dependecies required in all application. The dependencies are declared as dependency management to be included where are needed. It is also defined all the versions for all the dependencies in properties section to be more easy change some dependency version. For example the spring version tag is used in all spring dependencies. It is also defined all the plugins required in application as plugin dependency to share plugin configuration across all project modules.

3. The common project contains the Base classes for api response, service filter, service validator and service workflow. It was created this "base" notation to ensure that all projects follow the same architecture. The base api response contains the simples fields that will exist in all responses of all web services. The base validator is an interface that ensures that is implemented a validation method (for example to validate query parameters or request body). This base validator is extended by base workflow interface that is implemented by all web services workflow. The base workflow interface has also a method to get the validator that is typed in workflow. In this way all workflows implemented will have to implement a typed validator. This project contains also a string pool and a custom exception.

4. The logs project is responsible by all the logging in the application. It is stored in memory a mapping of all the logger classes and the respective log instances. Every time that a new class tries to log something, it is created a new log instance and save in memory. In the following log requests, it will be used the instance in memory. There are two methods, one to log messages and other to log exceptions. The logs are saved in folder ```logs``` in the path where the application is launched (this is to avoid permission error writing the logs)

5. The exchange rate project is an integration with the external api exchange rate ("https://exchangeratesapi.io"). This project contains an api that represents the external service response. It contains also a bridge that is responsible for all communications and a cache manager. The cache manager is again one mapping in memory that saves the exchange rate for some currency. Every time that is fetched an exchange rate, it is checked if exists in memory, and if exists it is checked if is valid. To check if cache is valid is analyzed the exchange rate day, because the value is updated every day. The bridge is implemented as singleton to reuse always the same instance in all communications with external partener.

6. The project flights-client, as the name represents, is a client to flights project. It contains the interface that exposes the web services implemented in the project and the apis to interact with the web services. In this way any micro-service or any partner that want to consume our flights api, import just this project, and does not know anything about the implementation.

7. Flights db project is the project responsible to interact with the database (postgres). All the database related logic (entities, entity manager, named queries) are defined in this project. The connection with the database is through JPA.

8. For last, the flights project is where is implemented the web services requested. This project contains a bridge to communicate with skypicker (also in singleton like the exchange rate bridge). The init of the application is through spring. It is also implemented an HTTP interceptor to log all the requests (as requested in plus section). It was also requested to validate the airline codes, to do that it is performed one call to skypicker locations api. Is with this service response that we know the airport name. To know the average prices, it is performed a second call to skypicker, but now to flights api.

# Junits

There are some projects with junits. The junits implemented are testing the best case scenario, and are also exploring the method to try break the flow.

# Resume
All the requirements were implemented

In plus section was implemented:
1. Cache on api
2. Logs on api
3. Two extra endpoints to access database

It is missing from plus section:
1. Docker image
2. Integration tests

## Setup
1. After clone the project ensure that you have java 1.8 and posgres
2. Adapt persistence.xml settings to connect correctly to your database. The file is located in flights-db project under src/main/resources.
3. Create the following table in public schema (adapting database_user_access tag:

```sql
CREATE TABLE public.ws_requests
(
    id integer SERIAL NOT NULL,
    http_method character varying(10) NOT NULL,
    endpoint character varying(100) NOT NULL,
    query_params character varying(200) NOT NULL,
    CONSTRAINT ws_requests_pkey PRIMARY KEY (id)
)
ALTER TABLE public.ws_requests
    OWNER to <database_user_access>;
```

4. Compile and start application (it starts in port 9080, update application.properties if need to change)

```bash
cd <git_cloned>
cd parent
mvn clean install
cd ../modules
mvn clean install
cd ../flights/target
java -jar flights-0.0.1-SNAPSHOT.jar
```

5. Services exposed:

```
GET localhost:9080/api/v1/flight/avg?dest=LIS&dest=OPO&curr=USD&dateFrom=2020/1/17&dateTo=2020/1/18
GET localhost:9080/api/v1/flight/logs
DELETE localhost:9080/api/v1/flight/logs
```

