package pt.cocus.flights.workflow;

import static pt.cocus.common.constant.Constant.AIRPORT;
import static pt.cocus.common.constant.Constant.COMMA;
import static pt.cocus.common.constant.Constant.EMPTY_STRING;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import pt.cocus.common.api.ResponseError;
import pt.cocus.common.filter.BaseFilter;
import pt.cocus.common.workflow.BaseWorkflow;
import pt.cocus.exchange.rate.api.ExchangeRateApi;
import pt.cocus.exchange.rate.bridge.ExchangeRateBridge;
import pt.cocus.flights.api.AvgData;
import pt.cocus.flights.api.SkypickerFlightApi;
import pt.cocus.flights.api.SkypickerFlightApi.Data;
import pt.cocus.flights.api.SkypickerLocationApi;
import pt.cocus.flights.api.SkypickerLocationApi.Location;
import pt.cocus.flights.bridge.SkypickerBridge;
import pt.cocus.flights.client.api.FlightsAvgApi;
import pt.cocus.flights.client.api.FlightsAvgApi.AirportAvgApi;
import pt.cocus.flights.client.api.FlightsAvgApi.AirportAvgApi.BagsAvgApi;
import pt.cocus.flights.filter.FlightsAvgFilter;
import pt.cocus.flights.message.BridgeMessage;
import pt.cocus.flights.message.ValidationMessage;
import pt.cocus.flights.validator.FlightsAvgValidator;
import pt.cocus.logs.util.CocusLogging;

/**
 * Implementation of Flights workflow
 * 
 * @author andrerodrigues
 *
 */
public class FlightsAvgWorkflow implements BaseWorkflow<FlightsAvgValidator> {
	private static final String CURRENCY_TO_RETURN = "EUR";
	private FlightsAvgValidator flightsAvgValidator;

	/**
	 * Implementation of retrieve avg information workflow
	 * 
	 * @param filter filters requested
	 * @param error error instance
	 * @return
	 */
	public FlightsAvgApi retrieveAvgInformation(FlightsAvgFilter filter, ResponseError error) {
		// Validate input data
		if (!validate(filter, error)) {
			return null;
		}

		// Fetch locations
		List<String> flightCodes = new ArrayList<>(2);
		flightCodes.addAll(filter.getDest());
		if (filter.getOrig() != null) {
			flightCodes.add(filter.getOrig());
		}

		Map<String, String> codeNameMapping = fetchLocation(flightCodes, error);
		if (codeNameMapping == null) {
			return null;
		}

		// Fetch data from external partner
		SkypickerFlightApi skypickerFlights = SkypickerBridge.getInstance().retrieveFlights(filter.getOrig(),
				filter.getDest(), filter.getCurr(), filter.getDateFrom(), filter.getDateTo());
		if (skypickerFlights == null) {
			return null;
		}

		// Compute avg data and fill api
		FlightsAvgApi api = new FlightsAvgApi();
		codeNameMapping.remove(filter.getOrig());
		fillAvgData(skypickerFlights.getData(), api, codeNameMapping, filter.getCurr());

		// Fill api response
		api.setCurrency(CURRENCY_TO_RETURN);
		api.setDateFrom(filter.getDateFrom());
		api.setDateTo(filter.getDateTo());
		return api;
	}

	/**
	 * Method that calculates the avg data for flight prices and bags price
	 * 
	 * @param dataList external data
	 * @param api response instance
	 * @param codeNameMapping airport code and name mapping
	 * @param curr currency requested
	 */
	private void fillAvgData(List<Data> dataList, FlightsAvgApi api, Map<String, String> codeNameMapping, String curr) {
		if (dataList == null) {
			CocusLogging.writeLog(getClass(), BridgeMessage.MISSING_DATA);
			return;
		}
		Map<String, AvgData> codeAvgMapping = new HashMap<>();
		for (Entry<String, String> code : codeNameMapping.entrySet()) {
			codeAvgMapping.put(code.getKey(), new AvgData());
		}

		for (Data data : dataList) {
			AvgData avg = codeAvgMapping.get(data.getFlyTo());
			avg.setBag1Avg(avg.getBag1Avg() + data.getBagsPrice().getFirst());
			avg.setBag2Avg(avg.getBag2Avg() + data.getBagsPrice().getSecond());
			avg.setPriveAvg(avg.getPriveAvg() + data.getPrice());
			avg.setItems(avg.getItems() + 1);
			codeAvgMapping.put(data.getFlyTo(), avg);
		}

		Float exchangeRate = 1F;
		// If data is not already in desired currency
		if (!CURRENCY_TO_RETURN.equals(curr)) {
			exchangeRate = computeExchangeRate(curr);
		}
		List<FlightsAvgApi.AirportAvgApi> airports = new LinkedList<>();
		for (Entry<String, AvgData> avgData : codeAvgMapping.entrySet()) {
			FlightsAvgApi.AirportAvgApi.BagsAvgApi bags = new BagsAvgApi();
			// Skypicker has a bug in their API. The bags price is not converted
			// to the currency requested...
			bags.setBag1Avg(Math.round(avgData.getValue().getBag1Avg() / avgData.getValue().getItems()));
			bags.setBag2Avg(Math.round(avgData.getValue().getBag2Avg() / avgData.getValue().getItems()));
			FlightsAvgApi.AirportAvgApi airport = new AirportAvgApi();
			airport.setBagsPriceAvg(bags);
			airport.setPriceAvg(Math.round(avgAndConvert(avgData.getValue().getPriveAvg(), avgData.getValue().getItems(), exchangeRate)));
			airport.setCode(avgData.getKey());
			airport.setName(codeNameMapping.get(avgData.getKey()));
			airports.add(airport);
		}
		api.setAirports(airports);
	}

	/**
	 * Calculates a value average and applies exchange rate
	 * 
	 * @param value original value
	 * @param items number of items
	 * @param exchangeRate exchange rate
	 * @return
	 */
	protected float avgAndConvert(Float value, int items, Float exchangeRate) {
		return value / exchangeRate / items;
	}

	/**
	 * Instantiate exchange rate service and retrieve exchange 
	 * rate for some currency.
	 * 
	 * @param curr currency to get exchange rate
	 * @return
	 */
	protected Float computeExchangeRate(String curr) {
		try {
			ExchangeRateApi exchangeRateApi = ExchangeRateBridge.getInstance().retrieveExchangeRate(curr);
			return exchangeRateApi.getRates().get(curr);
		} catch (Exception e) {
			CocusLogging.writeException(getClass(), e, e.getMessage());
		}
		return null;
	}

	/**
	 * Retrieve mapping of airports code to airports name. Receives the list
	 * of airports codes and retrieve from external partener the names
	 * 
	 * @param codes list of airport codes
	 * @param error error instance
	 * @return
	 */
	private Map<String, String> fetchLocation(List<String> codes, ResponseError error) {
		SkypickerLocationApi locationApi = SkypickerBridge.getInstance().retrieveLocations(codes);
		if (locationApi == null || locationApi.getLocations() == null || locationApi.getLocations().isEmpty()) {
			CocusLogging.writeLog(getClass(), ValidationMessage.ALL_INVALID_FLIGHT);
			error.setCode(String.valueOf(ValidationMessage.ALL_INVALID_FLIGHT.getCode()));
			error.setDetail(ValidationMessage.ALL_INVALID_FLIGHT.getDetail());
			return null;
		}

		Map<String, String> locations = new HashMap<>();
		String invalidFlight = EMPTY_STRING;
		for (Location flight : locationApi.getLocations()) {
			if (flight.isActive() && AIRPORT.equals(flight.getType())) {
				// Valid code
				locations.put(flight.getCode(), flight.getName());
			}
		}

		for (String code : codes) {
			if (locations.containsKey(code)) {
				continue;
			}
			if (!EMPTY_STRING.equals(invalidFlight)) {
				invalidFlight = invalidFlight.concat(COMMA);
			}
			invalidFlight = invalidFlight.concat(code);
		}

		if (!EMPTY_STRING.equals(invalidFlight)) {
			CocusLogging.writeLog(getClass(), ValidationMessage.INVALID_FLIGHT, invalidFlight);
			error.setCode(String.valueOf(ValidationMessage.INVALID_FLIGHT.getCode()));
			error.setDetail(MessageFormat.format(ValidationMessage.INVALID_FLIGHT.getDetailFormated(), invalidFlight));
		}

		return locations;
	}

	@Override
	public FlightsAvgValidator getValidator() {
		if (this.flightsAvgValidator == null) {
			this.flightsAvgValidator = new FlightsAvgValidator();
		}
		return this.flightsAvgValidator;
	}

	@Override
	public boolean validate(BaseFilter filter, ResponseError error) {
		return getValidator().validate((FlightsAvgFilter) filter, error);
	}
}
