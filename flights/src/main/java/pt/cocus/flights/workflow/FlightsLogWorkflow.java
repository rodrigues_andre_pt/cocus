package pt.cocus.flights.workflow;

import java.util.List;

import javax.persistence.EntityManager;

import pt.cocus.common.api.ResponseError;
import pt.cocus.common.filter.BaseFilter;
import pt.cocus.common.workflow.BaseWorkflow;
import pt.cocus.flights.client.api.FlightsLogApi;
import pt.cocus.flights.db.dao.WsRequestDao;
import pt.cocus.flights.db.entity.WsRequest;
import pt.cocus.flights.db.entity.manager.WsRequestEntityManager;
import pt.cocus.flights.mapper.LogDaoApiMapper;

/**
 * Implementation of Flights log workflow
 * 
 * @author andrerodrigues
 *
 */
public class FlightsLogWorkflow implements BaseWorkflow<FlightsLogApi> {
	private LogDaoApiMapper logDaoApiMapper;
	private WsRequestDao dao;
	
	@Override
	public boolean validate(BaseFilter filter, ResponseError error) {
		// No validation reqired
		return true;
	}

	@Override
	public FlightsLogApi getValidator() {
		return null;
	}

	/**
	 * Method to retreave all logs from database
	 * 
	 * @return
	 */
	public FlightsLogApi retrieveLogs() {
		EntityManager entitymanager = WsRequestEntityManager.getEntityManager();
		List<WsRequest> entries = entitymanager.createNamedQuery("WsRequest.findAll", WsRequest.class).getResultList();
		return getMapper().getWsRequest(entries);
	}

	/**
	 * Method to delete all logs from database
	 */
	public void deleteLogs() {
		getWsRequestDao().executeNamedQuery("WsRequest.deleteAll");
	}
	
	/**
	 * Lazy load of LogDaoApiMapper
	 * 
	 * @return
	 */
	private LogDaoApiMapper getMapper() {
		if (logDaoApiMapper == null) {
			logDaoApiMapper = new LogDaoApiMapper();
		}
		return logDaoApiMapper;
	}

	/**
	 * Lazy load of WsRequestDao
	 * 
	 * @return
	 */
	private WsRequestDao getWsRequestDao() {
		if (dao == null) {
			dao = new WsRequestDao();
		}
		return dao;
	}
}
