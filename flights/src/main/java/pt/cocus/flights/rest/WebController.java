package pt.cocus.flights.rest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Class that implements Spring MVC to register our HTTP intercepter
 * 
 * @author andrerodrigues
 *
 */
@EnableWebMvc
@Configuration
@ComponentScan
public class WebController implements WebMvcConfigurer {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new HttpInterceptor());
	}
}
