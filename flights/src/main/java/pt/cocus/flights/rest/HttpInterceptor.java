package pt.cocus.flights.rest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import pt.cocus.flights.db.dao.WsRequestDao;
import pt.cocus.flights.db.entity.WsRequest;

/**
 * Class that intercepts all HTTP requests and responses. Currently it is handled
 * just the requests
 * 
 * @author andrerodrigues
 *
 */
public class HttpInterceptor extends HandlerInterceptorAdapter {
	private WsRequestDao dao;
	
	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
			throws Exception {
		// Store WS requests in database asynchronously
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(() -> {
			WsRequest entity = new WsRequest();
			entity.setHttpMethod(request.getMethod());
			entity.setEndpoint(request.getRequestURI());
			entity.setQueryParams(request.getQueryString());

			getWsRequestDao().persist(entity);
		});

		return true;
	}
	
	/**
	 * Lazy load of WsRequestDao
	 * 
	 * @return
	 */
	private WsRequestDao getWsRequestDao() {
		if (dao == null) {
			dao = new WsRequestDao();
		}
		return dao;
	}
}
