package pt.cocus.flights.rest;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pt.cocus.common.api.BaseResponseApi;
import pt.cocus.common.api.ResponseError;
import pt.cocus.flights.client.api.FlightsAvgApi;
import pt.cocus.flights.client.api.FlightsLogApi;
import pt.cocus.flights.client.interfaces.FlightsWs;
import pt.cocus.flights.filter.FlightsAvgFilter;
import pt.cocus.flights.workflow.FlightsAvgWorkflow;
import pt.cocus.flights.workflow.FlightsLogWorkflow;
import pt.cocus.logs.util.CocusLogging;

/**
 * Implementation of Flights WS
 * 
 * @author andrerodrigues
 */
@RestController
@RequestMapping(value = "/api/v1")
public class FlightsWsImpl implements FlightsWs {
	private FlightsAvgWorkflow flightsAvgWorkflow;
	private FlightsLogWorkflow flightsLogWorkflow;

	@GetMapping(value = "/flight/avg")
	@Override
	public ResponseEntity<FlightsAvgApi> avgInformation(String dest, String orig, String dateFrom, String dateTo,
			String curr) {
		List<String> destinations = new LinkedList<>();
		if (dest != null) {
			destinations = Arrays.asList(dest.split("\\s*,\\s*"));
		}
		FlightsAvgFilter filter = new FlightsAvgFilter(destinations, orig, dateFrom, dateTo, curr);
		FlightsAvgApi response;
		ResponseError error = new ResponseError();
		try {
			response = getFlightsAvgWorkflow().retrieveAvgInformation(filter, error);
			if (response == null) {
				response = new FlightsAvgApi();
			}
			if (!StringUtils.isEmpty(error.getCode()) || !StringUtils.isEmpty(error.getDetail())) {
				response.setSuccess(false);
				response.setError(error);
				return ResponseEntity.badRequest().body(response);
			}
			response.setSuccess(true);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			response = new FlightsAvgApi();
			response.setSuccess(false);
			error.setDetail(ex.getMessage());
			response.setError(error);
			CocusLogging.writeException(getClass(), ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
	}

	@GetMapping(value = "/flight/logs")
	@Override
	public ResponseEntity<FlightsLogApi> retrieveLogs() {
		FlightsLogApi response;
		try {
			response = getFlightsLogWorkflow().retrieveLogs();
			response.setSuccess(true);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			response = new FlightsLogApi();
			response.setSuccess(false);
			ResponseError error = new ResponseError();
			error.setDetail(ex.getMessage());
			response.setError(error);
			CocusLogging.writeException(getClass(), ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
	}

	@DeleteMapping(value = "/flight/logs")
	@Override
	public ResponseEntity<BaseResponseApi> deleteLogs() {
		BaseResponseApi response = new BaseResponseApi();
		try {
			getFlightsLogWorkflow().deleteLogs();
			response.setSuccess(true);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			response.setSuccess(false);
			ResponseError error = new ResponseError();
			error.setDetail(ex.getMessage());
			response.setError(error);
			CocusLogging.writeException(getClass(), ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
	}

	/**
	 * Lazy load of Flights workflow
	 * 
	 * @return
	 */
	private FlightsAvgWorkflow getFlightsAvgWorkflow() {
		if (this.flightsAvgWorkflow == null) {
			this.flightsAvgWorkflow = new FlightsAvgWorkflow();
		}
		return this.flightsAvgWorkflow;
	}

	/**
	 * Lazy load of Flights WS Log
	 * 
	 * @return
	 */
	private FlightsLogWorkflow getFlightsLogWorkflow() {
		if (this.flightsLogWorkflow == null) {
			this.flightsLogWorkflow = new FlightsLogWorkflow();
		}
		return this.flightsLogWorkflow;
	}
}
