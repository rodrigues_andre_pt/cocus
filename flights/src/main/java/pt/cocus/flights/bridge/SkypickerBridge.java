package pt.cocus.flights.bridge;

import static pt.cocus.common.constant.Constant.AND_CODE;
import static pt.cocus.common.constant.Constant.COMMA;
import static pt.cocus.common.constant.Constant.CURR;
import static pt.cocus.common.constant.Constant.DATE_FROM;
import static pt.cocus.common.constant.Constant.DATE_TO;
import static pt.cocus.common.constant.Constant.EQUALS;
import static pt.cocus.common.constant.Constant.FLY_FROM;
import static pt.cocus.common.constant.Constant.FLY_TO;
import static pt.cocus.common.constant.Constant.ID;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.google.gson.Gson;

import pt.cocus.common.exception.CocusException;
import pt.cocus.common.util.DateUtil;
import pt.cocus.common.util.DateUtil.DatePattern;
import pt.cocus.flights.api.SkypickerFlightApi;
import pt.cocus.flights.api.SkypickerLocationApi;
import pt.cocus.flights.message.BridgeMessage;
import pt.cocus.flights.message.ValidationMessage;
import pt.cocus.logs.util.CocusLogging;

/**
 * Singleton class that communicates with external api (Skypicker)
 * 
 * @author andrerodrigues
 *
 */
public class SkypickerBridge {
	private static final String LOCATIONS_URL = "https://api.skypicker.com/locations?type=id";
	private static final String FLIGHTS_URL = "https://api.skypicker.com/flights?partner=pick";
	private static final int HTTP_CONNECT_TIMEOUT = 5000;
	private static final int HTTP_READ_TIMEOUT = 5000;
	
	private static SkypickerBridge instance = null;
	private Gson gson;

	private SkypickerBridge() {
		// Avoid initialization, force use getInstance
	}

	/**
	 * Creates class instance
	 * 
	 * @return
	 */
	public static SkypickerBridge getInstance() {
		if (instance == null) {
			synchronized (SkypickerBridge.class) {
				if (instance == null) {
					instance = new SkypickerBridge();
				}
			}
		}
		return instance;
	}

	/**
	 * Method to retrieve locations from External api
	 * 
	 * @param codes list of airport codes
	 * @return
	 */
	public SkypickerLocationApi retrieveLocations(List<String> codes) {
		if (codes == null || codes.isEmpty()) {
			CocusLogging.writeLog(getClass(), BridgeMessage.MISSING_LOCATIONS);
			return null;
		}

		StringBuilder targetUrl = new StringBuilder(LOCATIONS_URL);
		for (String code : codes) {
			targetUrl.append(AND_CODE).append(ID).append(EQUALS).append(code);
		}

		try {
			String content = callSkypicker(targetUrl.toString());
			if (content != null) {
				return getGson().fromJson(content, SkypickerLocationApi.class);
			}
		} catch (CocusException e) {
			CocusLogging.writeException(getClass(), e, e.getLocalizedMessage());
		}
		return null;
	}

	/**
	 * Method that performs the HTTP call to Skypicker
	 * 
	 * @param targetUrl url to call
	 * @return
	 * @throws CocusException
	 */
	protected String callSkypicker(String targetUrl) throws CocusException {
		try {
			URL url = new URL(targetUrl);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod(HttpMethod.GET.name());
			con.setRequestProperty(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.getType());
			con.setConnectTimeout(HTTP_CONNECT_TIMEOUT);
			con.setReadTimeout(HTTP_READ_TIMEOUT);
			con.setInstanceFollowRedirects(false);
			int status = con.getResponseCode();
			if (HttpStatus.OK.value() != status) {
				CocusLogging.writeLog(getClass(), BridgeMessage.ERROR_CALLING, url);
				return null;
			}
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuilder content = new StringBuilder();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();
			con.disconnect();
			return content.toString();
		} catch (Exception e) {
			throw new CocusException(new StringBuilder("Error calling: ").append(targetUrl).toString(), e);
		}
	}

	/**
	 * Method that retrieves flights information from Skypicker
	 * 
	 * @param from origin airport
	 * @param to list of destination airports
	 * @param curr currency code to get all the prices
	 * @param dateFrom start date
	 * @param dateTo end date
	 * @return
	 */
	public SkypickerFlightApi retrieveFlights(String from, List<String> to, String curr, String dateFrom,
			String dateTo) {
		if (to == null || to.isEmpty()) {
			CocusLogging.writeLog(getClass(), ValidationMessage.INVALID_PARAMETER, FLY_TO);
			return null;
		}
		if (StringUtils.isEmpty(curr)) {
			CocusLogging.writeLog(getClass(), ValidationMessage.INVALID_PARAMETER, CURR);
			return null;
		}

		try {
			StringBuilder targetUrl = new StringBuilder(FLIGHTS_URL);
			if (from != null) {
				targetUrl.append(AND_CODE).append(FLY_FROM).append(EQUALS).append(from);
			}
			targetUrl.append(AND_CODE).append(FLY_TO).append(EQUALS);
			int toSize = to.size();
			for (int i = 0; i < toSize; i++) {
				targetUrl.append(to.get(i));
				if (i != toSize - 1) {
					targetUrl.append(COMMA);
				}
			}
			targetUrl.append(AND_CODE).append(CURR).append(EQUALS).append(curr);
			String dateFromFormated = DateUtil.convertFromToFormat(dateFrom, DatePattern.YEAR_MONTH_DAY,
					DatePattern.DAY_MONTH_YEAR);
			targetUrl.append(AND_CODE).append(DATE_FROM).append(EQUALS).append(dateFromFormated);
			String dateToFormated = DateUtil.convertFromToFormat(dateTo, DatePattern.YEAR_MONTH_DAY,
					DatePattern.DAY_MONTH_YEAR);
			targetUrl.append(AND_CODE).append(DATE_TO).append(EQUALS).append(dateToFormated);

			String content = callSkypicker(targetUrl.toString());
			if (content != null) {
				return getGson().fromJson(content, SkypickerFlightApi.class);
			}
		} catch (Exception e) {
			CocusLogging.writeException(getClass(), e, e.getLocalizedMessage());
		}
		return null;

	}

	/**
	 * Lazy load of Gson
	 * 
	 * @return
	 */
	private Gson getGson() {
		if (gson == null) {
			gson = new Gson();
		}
		return gson;
	}
}
