package pt.cocus.flights.api;

import java.util.List;

/**
 * Class that represents the response of Skypicker Locations 
 * filtering by id. It is only mapped the fields used in our
 * workflow.
 * 
 * @author andrerodrigues
 * 
 */
public class SkypickerLocationApi {
	private List<Location> locations;

	/**
	 * @return the locations
	 */
	public List<Location> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public class Location {

		private boolean active;
		private String code;
		private String type;
		private String name;


		/**
		 * @return the active
		 */
		public boolean isActive() {
			return active;
		}

		/**
		 * @param active the active to set
		 */
		public void setActive(boolean active) {
			this.active = active;
		}

		/**
		 * @return the code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * @param code the code to set
		 */
		public void setCode(String code) {
			this.code = code;
		}

		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
	}
}
