package pt.cocus.flights.api;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Class that represents the response of Skypicker Flights filtering by origin,
 * destination and currency. It is only mapped the fields used in our workflow.
 * 
 * @author andrerodrigues
 * 
 */
public class SkypickerFlightApi {
	private List<Data> data;
	private String currency;

	/**
	 * @return the data
	 */
	public List<Data> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<Data> data) {
		this.data = data;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public class Data {
		@SerializedName("bags_price")
		private BagsPrice bagsPrice;
		private int price;
		private String flyTo;

		/**
		 * @return the bagsPrice
		 */
		public BagsPrice getBagsPrice() {
			return bagsPrice;
		}

		/**
		 * @param bagsPrice the bagsPrice to set
		 */
		public void setBagsPrice(BagsPrice bagsPrice) {
			this.bagsPrice = bagsPrice;
		}

		/**
		 * @return the price
		 */
		public int getPrice() {
			return price;
		}

		/**
		 * @param price the price to set
		 */
		public void setPrice(int price) {
			this.price = price;
		}

		/**
		 * @return the flyTo
		 */
		public String getFlyTo() {
			return flyTo;
		}

		/**
		 * @param flyTo the flyTo to set
		 */
		public void setFlyTo(String flyTo) {
			this.flyTo = flyTo;
		}

		public class BagsPrice {

			@SerializedName("1")
			private float first;
			@SerializedName("2")
			private float second;

			/**
			 * @return the first
			 */
			public float getFirst() {
				return first;
			}

			/**
			 * @param first the first to set
			 */
			public void setFirst(float first) {
				this.first = first;
			}

			/**
			 * @return the second
			 */
			public float getSecond() {
				return second;
			}

			/**
			 * @param second the second to set
			 */
			public void setSecond(float second) {
				this.second = second;
			}
		}
	}
}
