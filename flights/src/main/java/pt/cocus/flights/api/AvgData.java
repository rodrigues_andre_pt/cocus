package pt.cocus.flights.api;

/**
 * Class to hold the average data.
 * 
 * @author andrerodrigues
 *
 */
public class AvgData {
	private Float priveAvg = Float.valueOf(0);
	private Float bag1Avg = Float.valueOf(0);
	private Float bag2Avg = Float.valueOf(0);
	private int items;

	/**
	 * @return the priveAvg
	 */
	public Float getPriveAvg() {
		return priveAvg;
	}

	/**
	 * @param priveAvg the priveAvg to set
	 */
	public void setPriveAvg(Float priveAvg) {
		this.priveAvg = priveAvg;
	}

	/**
	 * @return the bag1Avg
	 */
	public Float getBag1Avg() {
		return bag1Avg;
	}

	/**
	 * @param bag1Avg the bag1Avg to set
	 */
	public void setBag1Avg(Float bag1Avg) {
		this.bag1Avg = bag1Avg;
	}

	/**
	 * @return the bag2Avg
	 */
	public Float getBag2Avg() {
		return bag2Avg;
	}

	/**
	 * @param bag2Avg the bag2Avg to set
	 */
	public void setBag2Avg(Float bag2Avg) {
		this.bag2Avg = bag2Avg;
	}

	/**
	 * @return the items
	 */
	public int getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int items) {
		this.items = items;
	}
}
