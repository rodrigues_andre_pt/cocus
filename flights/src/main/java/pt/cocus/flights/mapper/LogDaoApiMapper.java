package pt.cocus.flights.mapper;

import java.util.LinkedList;
import java.util.List;

import pt.cocus.flights.client.api.FlightsLogApi;
import pt.cocus.flights.client.api.FlightsLogApi.FlightLog;
import pt.cocus.flights.db.entity.WsRequest;

/**
 * Mapper of/from log DAO to log API
 * 
 * @author andrerodrigues
 *
 */
public class LogDaoApiMapper {

	/**
	 * Get API based on DAO
	 * 
	 * @param entries
	 * @return
	 */
	public FlightsLogApi getWsRequest(List<WsRequest> entries) {
		FlightsLogApi api = new FlightsLogApi();
		List<FlightLog> logEntries = new LinkedList<>();
		if (entries != null && !entries.isEmpty()) {
			for (WsRequest logEntry : entries) {
				FlightsLogApi.FlightLog request = new FlightLog();
				request.setEndpoint(logEntry.getEndpoint());
				request.setHttpMethod(logEntry.getHttpMethod());
				request.setQueryParms(logEntry.getQueryParams());
				logEntries.add(request);
			}
		}
		api.setCalls(logEntries);
		return api;
	}
}
