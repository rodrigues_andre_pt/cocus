package pt.cocus.flights.message;

import pt.cocus.logs.message.Message;
import static pt.cocus.common.constant.Constant.LOG_PLACEHOLDER;
import static pt.cocus.common.constant.Constant.ERROR_PLACEHOLDER;

/**
 * Enumeration of error messages related to validators
 * 
 * @author andrerodrigues
 *
 */
public enum ValidationMessage implements Message {
	MISSING_MANDATORY_PARAMETER(1001, "Missing mandatory parameter {}", Severity.ERROR),
	INVALID_PARAMETER(1002, "The parameter {} is invalid", Severity.ERROR),
	INVALID_DATE_RANGE(1003, "The dateTo is before dateFrom", Severity.ERROR),
	INVALID_FLIGHT(1004, "Invalid flight(s) received {}", Severity.ERROR),
	ALL_INVALID_FLIGHT(1005, "All flights received are invalid", Severity.ERROR);

	private final int code;
	private final String detail;
	private Severity severity;

	private ValidationMessage(int code, String detail, Severity severity) {
		this.code = code;
		this.detail = detail;
		this.severity = severity;
	}

	@Override
	public String getDetail() {
		return detail;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public Severity getSeverity() {
		return severity;
	}
	
	public String getDetailFormated() {
		return getDetail().replaceAll(LOG_PLACEHOLDER, ERROR_PLACEHOLDER);
	}
}
