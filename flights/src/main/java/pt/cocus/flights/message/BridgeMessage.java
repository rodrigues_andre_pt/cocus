package pt.cocus.flights.message;

import pt.cocus.logs.message.Message;

/**
 * Enumeration of error messages related to bridges
 * 
 * @author andrerodrigues
 *
 */
public enum BridgeMessage implements Message {
	MISSING_LOCATIONS(2001, "Missing locations to validate in Skypicker", Severity.ERROR),
	ERROR_CALLING(2002, "Error calling external api at {}", Severity.ERROR),
	MISSING_DATA(2003, "Missing mandatory data from Skypicker", Severity.ERROR);

	private final int code;
	private final String detail;
	private Severity severity;

	private BridgeMessage(int code, String detail, Severity severity) {
		this.code = code;
		this.detail = detail;
		this.severity = severity;
	}

	@Override
	public String getDetail() {
		return detail;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public Severity getSeverity() {
		return severity;
	}
}
