package pt.cocus.flights.validator;

import static pt.cocus.common.constant.Constant.CURR;
import static pt.cocus.common.constant.Constant.DATE_FROM;
import static pt.cocus.common.constant.Constant.DATE_TO;
import static pt.cocus.common.constant.Constant.DEST;

import java.text.MessageFormat;
import java.util.Currency;

import org.apache.commons.lang3.StringUtils;

import pt.cocus.common.api.ResponseError;
import pt.cocus.common.util.DateUtil;
import pt.cocus.common.util.DateUtil.DatePattern;
import pt.cocus.common.validator.BaseValidator;
import pt.cocus.flights.filter.FlightsAvgFilter;
import pt.cocus.flights.message.ValidationMessage;
import pt.cocus.logs.util.CocusLogging;

/**
 * Flights workflow validator.
 * It validates all the filters passed to WS.
 * 
 * @author andrerodrigues
 *
 */
public class FlightsAvgValidator implements BaseValidator<FlightsAvgFilter> {

	@Override
	public boolean validate(FlightsAvgFilter filter, ResponseError error) {
		if (filter == null || filter.getDest() == null || filter.getDest().isEmpty()) {
			CocusLogging.writeLog(getClass(), ValidationMessage.MISSING_MANDATORY_PARAMETER, DEST);
			error.setCode(String.valueOf(ValidationMessage.MISSING_MANDATORY_PARAMETER.getCode()));
			error.setDetail(MessageFormat.format(ValidationMessage.MISSING_MANDATORY_PARAMETER.getDetailFormated(), DEST));
			return false;
		}

		if (StringUtils.isEmpty(filter.getCurr())) {
			CocusLogging.writeLog(getClass(), ValidationMessage.MISSING_MANDATORY_PARAMETER, CURR);
			error.setCode(String.valueOf(ValidationMessage.MISSING_MANDATORY_PARAMETER.getCode()));
			error.setDetail(MessageFormat.format(ValidationMessage.MISSING_MANDATORY_PARAMETER.getDetailFormated(), CURR));
			return false;
		}

		if (!validCurrency(filter.getCurr())) {
			CocusLogging.writeLog(getClass(), ValidationMessage.INVALID_PARAMETER, CURR);
			error.setCode(String.valueOf(ValidationMessage.INVALID_PARAMETER.getCode()));
			error.setDetail(MessageFormat.format(ValidationMessage.INVALID_PARAMETER.getDetailFormated(), CURR));
			return false;
		}

		// Dates are not mandatory, but receiving one, is has to be received both
		if (StringUtils.isNotEmpty(filter.getDateFrom()) && StringUtils.isEmpty(filter.getDateTo())) {
			CocusLogging.writeLog(getClass(), ValidationMessage.MISSING_MANDATORY_PARAMETER, DATE_TO);
			error.setCode(String.valueOf(ValidationMessage.MISSING_MANDATORY_PARAMETER.getCode()));
			error.setDetail(MessageFormat.format(ValidationMessage.MISSING_MANDATORY_PARAMETER.getDetailFormated(), DATE_TO));
			return false;
		}
		if (StringUtils.isNotEmpty(filter.getDateTo()) && StringUtils.isEmpty(filter.getDateFrom())) {
			CocusLogging.writeLog(getClass(), ValidationMessage.MISSING_MANDATORY_PARAMETER, DATE_FROM);
			error.setCode(String.valueOf(ValidationMessage.MISSING_MANDATORY_PARAMETER.getCode()));
			error.setDetail(MessageFormat.format(ValidationMessage.MISSING_MANDATORY_PARAMETER.getDetailFormated(), DATE_FROM));
			return false;
		}

		if (StringUtils.isNotEmpty(filter.getDateTo()) && StringUtils.isNotEmpty(filter.getDateFrom())) {
			if (!DateUtil.validDate(filter.getDateTo(), DatePattern.YEAR_MONTH_DAY)) {
				CocusLogging.writeLog(getClass(), ValidationMessage.INVALID_PARAMETER, DATE_TO);
				error.setCode(String.valueOf(ValidationMessage.INVALID_PARAMETER.getCode()));
				error.setDetail(MessageFormat.format(ValidationMessage.INVALID_PARAMETER.getDetailFormated(), DATE_TO));
				return false;
			}

			if (!DateUtil.validDate(filter.getDateFrom(), DatePattern.YEAR_MONTH_DAY)) {
				CocusLogging.writeLog(getClass(), ValidationMessage.INVALID_PARAMETER, DATE_FROM);
				error.setCode(String.valueOf(ValidationMessage.INVALID_PARAMETER.getCode()));
				error.setDetail(MessageFormat.format(ValidationMessage.INVALID_PARAMETER.getDetailFormated(), DATE_FROM));
				return false;
			}

			if (!DateUtil.dateBeforeOf(filter.getDateFrom(), filter.getDateTo(), DatePattern.YEAR_MONTH_DAY)) {
				CocusLogging.writeLog(getClass(), ValidationMessage.INVALID_DATE_RANGE);
				error.setCode(String.valueOf(ValidationMessage.INVALID_DATE_RANGE.getCode()));
				error.setDetail(ValidationMessage.INVALID_DATE_RANGE.getDetail());
				return false;
			}
		}

		return true;
	}

	/**
	 * Method that validates if a currency code is valid
	 * 
	 * @param cur code to check
	 * @return
	 */
	protected boolean validCurrency(String cur) {
		if (cur == null) {
			return false;
		}
		try {
			return Currency.getInstance(cur) != null;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
}
