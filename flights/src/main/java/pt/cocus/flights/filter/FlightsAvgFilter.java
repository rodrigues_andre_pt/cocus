package pt.cocus.flights.filter;

import java.util.List;

import pt.cocus.common.filter.BaseFilter;

/**
 * Class that represents all the filters that can be used in Avg WS.
 * 
 * @author andrerodrigues
 *
 */
public class FlightsAvgFilter implements BaseFilter {
	private List<String> dest;
	private String orig;
	private String dateFrom;
	private String dateTo;
	private String curr;

	public FlightsAvgFilter(List<String> dest, String orig, String dateFrom, String dateTo, String curr) {
		this.dest = dest;
		this.orig = orig;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.curr = curr;
	}

	/**
	 * @return the dest
	 */
	public List<String> getDest() {
		return dest;
	}

	/**
	 * @param dest the dest to set
	 */
	public void setDest(List<String> dest) {
		this.dest = dest;
	}

	/**
	 * @return the orig
	 */
	public String getOrig() {
		return orig;
	}

	/**
	 * @param orig the orig to set
	 */
	public void setOrig(String orig) {
		this.orig = orig;
	}

	/**
	 * @return the dateFrom
	 */
	public String getDateFrom() {
		return dateFrom;
	}

	/**
	 * @param dateFrom the dateFrom to set
	 */
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the dateTo
	 */
	public String getDateTo() {
		return dateTo;
	}

	/**
	 * @param dateTo the dateTo to set
	 */
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @return the curr
	 */
	public String getCurr() {
		return curr;
	}

	/**
	 * @param curr the curr to set
	 */
	public void setCurr(String curr) {
		this.curr = curr;
	}
}
