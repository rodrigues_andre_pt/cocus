package pt.cocus.flights.workflow;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FlightsAvgWorkflowTest {
	private FlightsAvgWorkflow workflow = new FlightsAvgWorkflow();

	@Ignore
	public void TestRetrieveAvgInformation() {
		// To implement
	}

	@Ignore
	public void TestFillAvgData() {
		// To implement
	}

	@Test
	public void TestAvgAndConvert() {
		assertEquals(3.6022832F, workflow.avgAndConvert(102.24F, 23, 1.234F), 0);
	}

	public void TestComputeExchangeRate() {

	}

	@Ignore
	public void TestFetchLocation() {
		// To implement
	}
}
