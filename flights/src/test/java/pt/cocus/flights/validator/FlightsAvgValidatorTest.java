package pt.cocus.flights.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import pt.cocus.common.api.ResponseError;
import pt.cocus.flights.filter.FlightsAvgFilter;

/**
 * Test class for {@link FlightsAvgValidator}
 * 
 * @author andrerodrigues
 *
 */
public class FlightsAvgValidatorTest {
	private FlightsAvgValidator validator = new FlightsAvgValidator();
	
	@Test
	public void TestValidCurrency() {
		assertTrue(validator.validCurrency("EUR"));
		assertTrue(validator.validCurrency("USD"));
		assertFalse(validator.validCurrency("FAKE_CURRENCY"));
		assertFalse(validator.validCurrency(""));
		assertFalse(validator.validCurrency(null));
	}
	
	@Test
	public void TestValidate() {
		ResponseError error = new ResponseError();
		assertFalse(validator.validate(null, error));

		// Missing destination
		assertFalse(validator.validate(new FlightsAvgFilter(null, null, null, null, null), error));
		assertFalse(validator.validate(new FlightsAvgFilter(new LinkedList<>(), null, null, null, null), error));

		List<String> destination = new ArrayList<>(Arrays.asList("OPO"));
		// Missing currency
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, null, null, null), error));
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, null, null, ""), error));

		// Mixing dates
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, "FROM", null, "EUR"), error));
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, null, "TO", "EUR"), error));
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, "FROM", "TO", "EUR"), error));
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, "2010/01/02", "TO", "EUR"), error));
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, "FROM", "2010/01/02", "EUR"), error));
		assertFalse(validator.validate(new FlightsAvgFilter(destination, null, "2019/12/20", "2010/01/02", "EUR"), error));
		
		assertTrue(validator.validate(new FlightsAvgFilter(destination, null, null, null, "EUR"), error));
		assertTrue(validator.validate(new FlightsAvgFilter(destination, null, "2009/12/20", "2010/01/02", "EUR"), error));
		assertTrue(validator.validate(new FlightsAvgFilter(destination, "", "2009/12/20", "2010/01/02", "EUR"), error));
	}
}
