package pt.cocus.flights.client.api;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import pt.cocus.common.api.BaseResponseApi;

/**
 * Class that represents the Flights avg WS API
 * 
 * @author andrerodrigues
 *
 */
public class FlightsAvgApi extends BaseResponseApi {
	private String currency;
	private String dateFrom;
	private String dateTo;
	List<AirportAvgApi> airports;

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the dateFrom
	 */
	public String getDateFrom() {
		return dateFrom;
	}

	/**
	 * @param dateFrom the dateFrom to set
	 */
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the dateTo
	 */
	public String getDateTo() {
		return dateTo;
	}

	/**
	 * @param dateTo the dateTo to set
	 */
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @return the airports
	 */
	public List<AirportAvgApi> getAirports() {
		return airports;
	}

	/**
	 * @param airports the airports to set
	 */
	public void setAirports(List<AirportAvgApi> airports) {
		this.airports = airports;
	}

	public static class AirportAvgApi {
		private String code;
		private String name;
		@SerializedName("price_avg")
		private int priceAvg;
		@SerializedName("bags_price_avg")
		private BagsAvgApi bagsPriceAvg;

		/**
		 * @return the code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * @param code the code to set
		 */
		public void setCode(String code) {
			this.code = code;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the priceAvg
		 */
		public int getPriceAvg() {
			return priceAvg;
		}

		/**
		 * @param priceAvg the priceAvg to set
		 */
		public void setPriceAvg(int priceAvg) {
			this.priceAvg = priceAvg;
		}

		/**
		 * @return the bagsPriceAvg
		 */
		public BagsAvgApi getBagsPriceAvg() {
			return bagsPriceAvg;
		}

		/**
		 * @param bagsPriceAvg the bagsPriceAvg to set
		 */
		public void setBagsPriceAvg(BagsAvgApi bagsPriceAvg) {
			this.bagsPriceAvg = bagsPriceAvg;
		}

		public static class BagsAvgApi {
			@SerializedName("bag1_avg")
			private int bag1Avg;
			@SerializedName("bag2_avg")
			private int bag2Avg;

			/**
			 * @return the bag1Avg
			 */
			public int getBag1Avg() {
				return bag1Avg;
			}

			/**
			 * @param bag1Avg the bag1Avg to set
			 */
			public void setBag1Avg(int bag1Avg) {
				this.bag1Avg = bag1Avg;
			}

			/**
			 * @return the bag2Avg
			 */
			public int getBag2Avg() {
				return bag2Avg;
			}

			/**
			 * @param bag2Avg the bag2Avg to set
			 */
			public void setBag2Avg(int bag2Avg) {
				this.bag2Avg = bag2Avg;
			}

		}
	}
}
