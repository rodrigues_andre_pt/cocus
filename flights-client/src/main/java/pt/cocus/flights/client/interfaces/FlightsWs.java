package pt.cocus.flights.client.interfaces;

import static pt.cocus.common.constant.Constant.CURR;
import static pt.cocus.common.constant.Constant.DATE_FROM;
import static pt.cocus.common.constant.Constant.DATE_TO;
import static pt.cocus.common.constant.Constant.DEST;
import static pt.cocus.common.constant.Constant.ORIG;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

import pt.cocus.common.api.BaseResponseApi;
import pt.cocus.flights.client.api.FlightsAvgApi;
import pt.cocus.flights.client.api.FlightsLogApi;

/**
 * Interface that defines the available Flights WS
 * 
 * @author andrerodrigues
 * 
 */
public interface FlightsWs {
	
	/**
	 * Retrieves average information filtered by origin and destination airport
	 * 
	 * @param dest destination airport
	 * @param orig origin airport
	 * @param dateFrom start date
	 * @param dateTo end date
	 * @param curr currency code
	 * @return
	 */
	public ResponseEntity<FlightsAvgApi> avgInformation(@RequestParam(name = DEST, required = false) String dest,
			@RequestParam(name = ORIG, required = false) String orig,
			@RequestParam(name = DATE_FROM, required = false) String dateFrom,
			@RequestParam(name = DATE_TO, required = false) String dateTo,
			@RequestParam(name = CURR, required = false) String curr);
	
	/**
	 * Retrieves the log of all WS calls
	 * 
	 * @return
	 */
	public ResponseEntity<FlightsLogApi> retrieveLogs();
	
	/**
	 * Deletes the log of all WS calls
	 * 
	 * @return
	 */
	public ResponseEntity<BaseResponseApi> deleteLogs();
}
