package pt.cocus.flights.client.api;

import java.util.List;

import pt.cocus.common.api.BaseResponseApi;

/**
 * Class that represents the api to retrieve the log of WS calls
 * 
 * @author andrerodrigues
 *
 */
public class FlightsLogApi extends BaseResponseApi {
	List<FlightLog> calls;

	/**
	 * @return the calls
	 */
	public List<FlightLog> getCalls() {
		return calls;
	}

	/**
	 * @param calls the calls to set
	 */
	public void setCalls(List<FlightLog> calls) {
		this.calls = calls;
	}
	
	public static class FlightLog {
		private String httpMethod;
		private String endpoint;
		private String queryParms;
		/**
		 * @return the httpMethod
		 */
		public String getHttpMethod() {
			return httpMethod;
		}
		/**
		 * @param httpMethod the httpMethod to set
		 */
		public void setHttpMethod(String httpMethod) {
			this.httpMethod = httpMethod;
		}
		/**
		 * @return the endpoint
		 */
		public String getEndpoint() {
			return endpoint;
		}
		/**
		 * @param endpoint the endpoint to set
		 */
		public void setEndpoint(String endpoint) {
			this.endpoint = endpoint;
		}
		/**
		 * @return the queryParms
		 */
		public String getQueryParms() {
			return queryParms;
		}
		/**
		 * @param queryParms the queryParms to set
		 */
		public void setQueryParms(String queryParms) {
			this.queryParms = queryParms;
		}
	}
}
