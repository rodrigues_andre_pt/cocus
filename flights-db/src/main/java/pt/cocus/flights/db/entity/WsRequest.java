package pt.cocus.flights.db.entity;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ws_requests database table.
 * 
 * @author andrerodrigues
 *
 */
@Entity
@Table(name = "ws_requests")
@NamedQueries({ @NamedQuery(name = "WsRequest.findAll", query = "SELECT w FROM WsRequest w"),
		@NamedQuery(name = "WsRequest.deleteAll", query = "DELETE FROM WsRequest") })
@Cacheable(false)
public class WsRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String endpoint;

	@Column(name = "http_method")
	private String httpMethod;

	@Column(name = "query_params")
	private String queryParams;

	public WsRequest() {
		// Default constructor
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEndpoint() {
		return this.endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getHttpMethod() {
		return this.httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getQueryParams() {
		return this.queryParams;
	}

	public void setQueryParams(String queryParams) {
		this.queryParams = queryParams;
	}

	@Override
	public String toString() {
		return "WsRequest [id=" + id + ", httpMethod=" + httpMethod + ", endpoint=" + endpoint + ", queryParams="
				+ queryParams + "]";
	}

}