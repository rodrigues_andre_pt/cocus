package pt.cocus.flights.db.dao;

import javax.persistence.EntityManager;

import pt.cocus.flights.db.entity.WsRequest;
import pt.cocus.flights.db.entity.manager.WsRequestEntityManager;

/**
 * Class that performs database opertions in WsRequest entity
 * 
 * @author andrerodrigues
 *
 */
public class WsRequestDao {
	/**
	 * Method to store a entity
	 * 
	 * @param entity entity to persist
	 */
	public void persist(WsRequest entity) {
		EntityManager entitymanager = WsRequestEntityManager.getEntityManager();
		entitymanager.getTransaction().begin();
		entitymanager.persist(entity);
		entitymanager.getTransaction().commit();
		entitymanager.close();
	}
	
	/**
	 * Method to execute a query
	 * 
	 * @param query query to execute
	 */
	public void executeNamedQuery(String query) {
		EntityManager entitymanager = WsRequestEntityManager.getEntityManager();
		entitymanager.getTransaction().begin();
		entitymanager.createNamedQuery(query).executeUpdate();
		entitymanager.getTransaction().commit();
		entitymanager.close();
	}
}
