package pt.cocus.flights.db.entity.manager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Entity manager for persistence unit flights-db
 * 
 * @author andrerodrigues
 *
 */
public class WsRequestEntityManager {
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("flights-db");

	private WsRequestEntityManager() {
		// Avoid initialization
	}

	public static EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public void closeEntityManager(EntityManager entityManager) {
		entityManager.close();
	}
}
